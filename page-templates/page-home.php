<?php
/*
Template Name: Home
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div id="page-home" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
    <div class="row">
      <div class="medium-10 medium-centered">
        <div class="row entry-content">
          <div class="large-4 columns show-for-large">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/wta-image-door-home.png" alt="logo">
          </div>
          <div class="large-8 columns">
            <header>
                <h2 class="entry-title"><?php the_title(); ?></h2>
            </header>

            <?php the_content(); ?>
          </div>
        </div>

        <div class="row collapse">
          <div class="medium-4 columns home-btn" id="history">
            <h6>History</h6>
          </div>
          <div class="medium-4 columns home-btn" id="portfolio">
            <h6>Portfolio</h6>
          </div>
          <div class="medium-4 columns home-btn" id="directions">
            <h6>Products</h6>
          </div>
        </div>
      </div>
    </div>

      <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
  </article>
<?php endwhile;?>

<section class="sponsors">
  <div class="row">
    <div class="medium-6 medium-centered columns">
      <?php 
        $sponsors = array (
           'post_type' => 'sponsor',
           'posts_per_page' => -1,
           'orderby' => 'menu_order',
           'order' => 'DSC'
        );
        query_posts($sponsors);
      ?>
      <div class="row sponsor-list">
        <?php while (have_posts()) : the_post(); ?>
            <div class="small-6 medium-3 columns">
              <a href="<?php echo types_render_field( "sponsor-website", array('output' => 'raw') ) ?>" target="_blank">
                <?php echo types_render_field( "sponsor-logo", array( "alt" => "sponsor logo") ) ?>
              </a>
            </div>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
</section>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer();

<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link href="https://fonts.googleapis.com/css?family=Fira+Sans|Work+Sans" rel="stylesheet">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'foundationpress_after_body' ); ?>

	<div class="mobile-top-bar">
		<div class="row">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="mobile-nav-logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/wta-logo-horz-reg.png" alt="logo">
			</a>

			<div class="mobile-nav-icon">
				<a href="javascript:;"><i class="fa fa-bars"></i></a>
			</div>

			<div class="mobile-nav">
				<i class="fa fa-times close"></i>
				<div class="mobile-nav-links">
					<?php wp_nav_menu( array('menu' => 'Mobile' )); ?>
				</div>
			</div>
		</div>
	</div>

	<header id="masthead" class="desktop-header" role="banner">
		<nav id="site-navigation" class="main-navigation top-bar" role="navigation">
			<div class="row collapse">
				<div class="large-4 columns">
					<?php foundationpress_top_bar_l(); ?>
				</div>
				<div class="large-4 columns logo">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/wta-logo-horz-reg.png" alt="logo">
				</div>
				<div class="large-4 columns">
					<?php foundationpress_top_bar_r(); ?>
				</div>
			</div>
		</nav>
	</header>

	<section class="container">
		<?php do_action( 'foundationpress_after_header' );

<?php
/*
Template Name: Products
*/

 get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div id="page" role="main">
    <?php do_action( 'foundationpress_before_content' ); ?>
    <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="row">
              <div class="medium-8 medium-centered columns">
                <header>
                  <h2 class="sub-title"><?php the_title(); ?></h2>
                </header>
              </div>
            </div>

            <section class="product-cats">
                <div class="row">
                    <div class="medium-3 columns">
                        <a href="/wood-doors/">
                            <div class="product-cat" id="doors">
                                <div class="cat-overlay"></div>
                                <h4>Wood Doors</h4>
                            </div>
                        </a>
                    </div>
                    <div class="medium-3 columns">
                        <a href="/hollow-metal-doors/">            
                            <div class="product-cat" id="hollow">
                                <div class="cat-overlay"></div>
                                <h4>Hollow Metal Doors</h4>
                            </div>
                        </a>
                    </div>
                    <div class="medium-3 columns">
                        <a href="/specialty-doors/">
                            <div class="product-cat" id="specialty">
                                <div class="cat-overlay"></div>
                                <h4>Specialty Doors</h4>
                            </div>
                        </a>
                    </div>
                    <div class="medium-3 columns">
                        <a href="/hardware/">
                            <div class="product-cat" id="hardware">
                                <div class="cat-overlay"></div>
                                <h4>Hardware</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </section>

            <div class="row">
                <div class="medium-8 medium-centered columns">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endwhile;?>
    </article>

    <?php do_action( 'foundationpress_after_content' ); ?>

</div>

 <?php get_footer();
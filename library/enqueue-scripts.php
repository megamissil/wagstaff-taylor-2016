<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_scripts' ) ) :
	function foundationpress_scripts() {

	// Enqueue the main Stylesheet.
	wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/assets/stylesheets/foundation.css', array(), '2.6.1', 'all' );

	// Deregister the jquery version bundled with WordPress.
	wp_deregister_script( 'jquery' );

	// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', false );

	// If you'd like to cherry-pick the foundation components you need in your project, head over to gulpfile.js and see lines 35-54.
	// It's a good idea to do this, performance-wise. No need to load everything if you're just going to use the grid anyway, you know :)
	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/javascript/foundation.js', array('jquery'), '2.6.1', true );

	// Add the comment-reply library on pages where it is necessary
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	}

	add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
endif;

//hide Sponsor fields from all pages except 'Home'
add_action('admin_enqueue_scripts', 'home_admin_styles');
function home_admin_styles() {
    $arr = array(5);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-sponsor {display:none;}
        </style>';
    }
}

//hide Product fields from all pages except 'Products'
add_action('admin_enqueue_scripts', 'product_admin_styles');
function product_admin_styles() {
    $arr = array(10);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-door {display:none;}
        </style>';
    }
}

//hide Staff fields from all pages except 'About Us'
add_action('admin_enqueue_scripts', 'staff_admin_styles');
function staff_admin_styles() {
    $arr = array(8);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-staff-member {display:none;}
        </style>';
    }
}

//hide Door Product fields from all pages except 'Wood Doors'
add_action('admin_enqueue_scripts', 'doors_admin_styles');
function doors_admin_styles() {
    $arr = array(88);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-wood-door {display:none;}
        </style>';
    }
}

//hide Hardware fields from all pages except 'Hardware'
add_action('admin_enqueue_scripts', 'hardware_admin_styles');
function hardware_admin_styles() {
    $arr = array(95);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-hardware-products {display:none;}
        </style>';
    }
}

//hide Hollow Metal Door fields from all pages except 'Hollow Metal Doors'
add_action('admin_enqueue_scripts', 'hollow_doors_admin_styles');
function hollow_doors_admin_styles() {
    $arr = array(91);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-hollow-metal-door {display:none;}
        </style>';
    }
}

//hide Specialty Door fields from all pages except 'Specialty Doors'
add_action('admin_enqueue_scripts', 'specialty_doors_admin_styles');
function specialty_doors_admin_styles() {
    $arr = array(93);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-specialty-door {display:none;}
        </style>';
    }
}
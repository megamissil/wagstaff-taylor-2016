<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

 <div id="page" role="main">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
  <?php while ( have_posts() ) : the_post(); ?>
    <div class="row">
      <div class="medium-8 medium-centered columns">
        <header>
          <h2 class="sub-title"><?php the_title(); ?></h2>
        </header>
<!--         <div class="sub-image">
          <?php the_post_thumbnail(); ?>
        </div> -->
        <div class="entry-content">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  <?php endwhile;?>
</article>
  <?php if ( is_page('about-us') ) { ?>
    <section class="staff">

          <div class="staff-row">
            <?php 
              $staff = array (
                 'post_type' => 'staff-member',
                 'posts_per_page' => 5,
                 'orderby' => 'menu_order',
                 'order' => 'DSC'
              );
              query_posts($staff);
            ?>

            <?php while (have_posts()) : the_post(); ?>
              <div class="staff-col">
                  <?php echo types_render_field( "staff-image", array( "alt" => "staff") ) ?>
                  <h4><?php echo types_render_field( "staff-name", array( "alt" => "staff") ) ?></h5>
                  <h6 class="staff-role"><?php echo types_render_field( "staff-role", array( "alt" => "staff") ) ?></h6>
                  <p><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo types_render_field( "staff-email", array( "alt" => "staff") ) ?></p>
                  <p><i class="fa fa-phone" aria-hidden="true"></i> <span><?php echo types_render_field( "staff-phone", array( "alt" => "staff") ) ?></span></p>
              </div>
            <?php endwhile; ?>
          </div>

    </section>
  <?php } ?>
 <?php do_action( 'foundationpress_after_content' ); ?>

 </div>

 <?php get_footer();
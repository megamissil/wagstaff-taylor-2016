<?php
/*
Template Name: Contact
*/

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

 <div id="page" role="main">

 <?php do_action( 'foundationpress_before_content' ); ?>
   <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
    <?php while ( have_posts() ) : the_post(); ?>
      <div class="row">
        <div class="medium-10 medium-centered columns">
          <header>
            <h2 class="sub-title"><?php the_title(); ?></h2>
          </header>
          
          <div class="row">
            <div class="medium-6 columns contact-form">
              <?php echo do_shortcode( '[contact-form-7 id="18" title="Contact form 1"]' ); ?>
            </div>
            <div class="medium-6 columns contact-info">
              <?php the_content(); ?>
            </div>
          </div>
        
        </div>
      </div>
    <?php endwhile;?>
  </article>
 <?php do_action( 'foundationpress_after_content' ); ?>

</div>

 <?php get_footer();
<?php
/*
Template Name: Product Category
*/

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

 <div id="page" role="main">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
    <div class="row">
      <div class="medium-8 medium-centered columns">
        <header>
          <h2 class="sub-title"><?php the_title(); ?></h2>
        </header>
      </div>
    </div>

    <section class="product-grid">
          <div class="row">
              <?php 
                if ( is_page('wood-doors') ) { 
                  $doors = array (
                     'post_type' => 'wood-door',
                     'posts_per_page' => -1,
                     'orderby' => 'menu_order',
                     'order' => 'DSC'
                  );

                  query_posts($doors);
                } elseif ( is_page('specialty-doors') ) {
                  $specialty = array (
                     'post_type' => 'specialty-door',
                     'posts_per_page' => -1,
                     'orderby' => 'menu_order',
                     'order' => 'DSC'
                  );

                  query_posts($specialty);
                } elseif ( is_page('hollow-metal-doors') ) {
                  $hollow = array (
                     'post_type' => 'hollow-metal-door',
                     'posts_per_page' => -1,
                     'orderby' => 'menu_order',
                     'order' => 'DSC'
                  );

                  query_posts($hollow);
                } elseif ( is_page('hardware') )  {
                  $hardware = array (
                     'post_type' => 'hardware',
                     'posts_per_page' => -1,
                     'orderby' => 'menu_order',
                     'order' => 'DSC'
                  );

                  query_posts($hardware);
                }
              ?>

              <?php while (have_posts()) : the_post(); ?>
                <div class="medium-3 columns company-col">
                  <div class="company-logo">
                    <a href="<?php echo types_render_field( "company-website", array('output' => 'raw') ) ?>" target="_blank">
                      <?php echo types_render_field( "logo", array( "alt" => "sponsor logo") ) ?>
                    </a>
                  </div>
                  <h5><?php echo types_render_field( "company-name", array('output' => 'raw') ) ?></h5>
                </div>
              <?php endwhile; ?>
          </div>
    </section>

    <div class="row">
      <div class="medium-8 medium-centered columns">
        <div class="product-content">
          <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
</article>

 <?php do_action( 'foundationpress_after_content' ); ?>

 </div>

 <?php get_footer();